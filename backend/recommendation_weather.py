import json
import requests
from backend.airport_listing import Airport_listing
from backend.show_data import  Show_data

import sys


class recommendation_weather:

    forecastDate = []
    dayName = []
    highTemperature = []
    lowTemperature = []
    phrase = []
    weather_forecast = []
    degree_sign = u"\N{DEGREE SIGN}"


    def __init__(self,airport_name):
        recommendation_weather.weather_forecast.clear()
        recommendation_weather.forecastDate.clear()
        recommendation_weather.dayName.clear()
        recommendation_weather.highTemperature.clear()
        recommendation_weather.lowTemperature.clear()
        recommendation_weather.phrase.clear()

        self.headers = {
            'x-apikey': '89e15931434731aefdaa04920ec60e44',
        }

        self.iataCode = self.iata_conversion(airport_name)
        #self.iataCode = airport_name
        print(self.iataCode)
        self.get_weather(self.iataCode)



    def url_airports(self, iataCode):
        return "https://weather-qa.api.aero/weather/v1/forecast/" + str(iataCode) + "?duration=7&temperatureScale=C"

    def get_weather(self, iataCode):
        response = requests.get(self.url_airports(iataCode), headers=self.headers)
        if response.status_code == 200:
            weather = json.loads(response.content.decode('utf-8'))

            try:

                if (weather["version"] == "v1"):
                    for weatherİnfo in weather["weatherForecast"]:
                            try:
                                self.forecastDate.append(weatherİnfo["forecastDate"])
                                self.dayName.append(weatherİnfo["dayName"])
                                self.highTemperature.append(weatherİnfo["highTemperatureValue"])
                                self.lowTemperature.append(weatherİnfo["lowTemperatureValue"])
                                self.phrase.append(weatherİnfo["phrase"])

                            except:
                                recommendation_weather.weather_forecast.clear()
                                recommendation_weather.weather_forecast.append("Weather Forecast Not Found!")
                                continue

                    for i in range(0, len(self.forecastDate)):
                        recommendation_weather.weather_forecast.append(self.forecastDate[i] + " " + self.dayName[i]
                                                                       + " " + self.highTemperature[i] + "/" +
                                                                       self.lowTemperature[i] + recommendation_weather.degree_sign + "C " + self.phrase[i])
                        print(recommendation_weather.weather_forecast[i])

                else:
                    recommendation_weather.weather_forecast.clear()
                    recommendation_weather.weather_forecast.append("Weather Forecast Not Found!")
                    sys.exit(0)

            except:
                recommendation_weather.weather_forecast.clear()
                recommendation_weather.weather_forecast.append("Weather Forecast Not Found!")
                pass




    @staticmethod
    def iata_conversion(airport_name):
        with open('C:\\Users\\Tuna\\PycharmProjects\\sitaozuterosc\\backend\\airports_with_cities.json') as json_file:
            airports = json.load(json_file)


        for key in airports.keys():
            for airport in airports[str(key)]:
                if airport_name == airport["name"]:
                    return airport["iataCode"]






if __name__ == '__main__':
    obj = recommendation_weather("Ataturka")


