import requests
import json
import pycountry

from sitaozuterosc.backend.show_data import Show_data
from sitaozuterosc.backend.airport_listing import Airport_listing

headers = {
    'x-apikey': '3035d833bb6e531654a3cce03e6b1fde',
}

url2 = "https://restcountries.eu/rest/v2/"
response = requests.get(url2)
data2 = response.text
parsed2 = json.loads(data2)
all_countries = []
all_codes = []

url = "https://data-qa.api.aero/data/v3/airports"
response = requests.get(url, headers=headers)
data = response.text
parsed = json.loads(data)
# parsed = str(data)
# splitted = parsed.split("{")
all_airports = []
all_airports_name = []
all_airports_country = []
all_airports_city = []


def get_country_by_code(country_code):
    for country in parsed2:
        all_countries.append(country["name"])
        all_codes.append(country["alpha2Code"])

        for x in range(0, len(all_codes)):
            if country_code == all_codes[x]:
                return all_countries[x]


# def get_elements(all_airports, how_far, needed, list):
#   name = ""
#  for dummy in all_airports:
#     y = dummy.find(needed)
#    for index in range(y + how_far, len(dummy)):
#       if dummy[index] == '\"':
#          break
#     else:
#        name += dummy[index]
#  if name != '':
#     list.append(name)

# for airport in splitted:
#   if airport != splitted[0] and airport != splitted[1] and airport[airport.find("iataCode") + 11] != "\"":
#      all_airports.append(airport)


cities_with_airports = dict()

# airports = Show_data.data_from_api_getter("airport")["airports"]
airports = Airport_listing.get_airports()

print(airports)

iataCodes = list()

for airport in airports:

    try:
        iataCode = airport["iataCode"]
    except:
        continue

    if iataCode == "" or iataCode in iataCodes:
        continue

    iataCodes.append(iataCode)

    city_attribute = "countryCode"

    try:
        country_str = pycountry.countries.get(alpha_2=airport[city_attribute]).name
    except:
        city_attribute = "country"
        country_str = airport[city_attribute]

    try:
        cities_with_airports[country_str].append(dict({"city":airport["city"], "iataCode":iataCode, "icaoCode":airport["icaoCode"], "name":airport["name"]}))
    except:
        cities_with_airports[country_str] = list()
        cities_with_airports[country_str].append(dict({"city":airport["city"], "iataCode":iataCode, "icaoCode":airport["icaoCode"], "name":airport["name"]}))

"""print(cities_with_airports["Turkey"])"""

with open("airports_with_cities.json", 'w') as file:
    file.write(json.dumps(cities_with_airports, sort_keys=True, indent=4))
