import json


class Airport_listing:

    def __init__(self, airport_json=None):
        if airport_json is not None:
            self.airport_json = airport_json
        else:
            with open('airports.json') as json_file:
                self.airport_json = json.load(json_file)

    def airport_getter(self, country_name, country_code="none", city_name="none"):
        for airport in self.airport_json["airports"]:
            city_check = (city_name == "none" or airport['city'] == city_name)

            airport_found_message = airport['name'] + " located in " + airport['city'] + " of Turkey with icao code " + \
                                    airport['icaoCode'] + " and iata code " + airport["iataCode"]
            try:
                if airport['country'] == country_name and city_check:
                    print(airport_found_message)
            except:
                if airport['countryCode'] == country_code and city_check:
                    print(airport_found_message)
        return "airport not found"

    def iata_getter_for_name(self, airport_name):
        for airport in self.airport_json["airports"]:

            try:
                if airport['name'] == airport_name:
                    return airport["iataCode"]

            except:
                continue

        return None

    @staticmethod
    def get_airports():
        with open('airports.json') as json_file:
            return json.load(json_file)["airports"]
