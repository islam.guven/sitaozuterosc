import json
import requests
from backend.airport_listing import Airport_listing

class Show_data:

    def __init__(self):
        pass

    @staticmethod
    def data_from_api_getter(mode, write_to_file=False, icao_code=""):

        api_dict = {

            "airport_api": '3035d833bb6e531654a3cce03e6b1fde',
            "weather_api": "89e15931434731aefdaa04920ec60e44"

        }

        url_dict = {

            "airport_url": 'https://data-qa.api.aero/data/v3/airports',
            "weather_url": "https://weather-qa.api.aero/weather/v1/forecast/" + icao_code + "?duration=7&temperatureScale=F"

        }

        headers = {
            'x-apikey': api_dict[mode + "_api"],
        }

        url = url_dict[mode + "_url"]
        response = requests.get(url, headers=headers)
        # print(response)
        if response.status_code == 200:
            data_json = json.loads(response.content.decode('utf-8'))

        if write_to_file:
            with open("weather.json", 'w') as file:
                file.write(json.dumps(data_json, sort_keys=True, indent=4))

        return data_json


"""airport_lister = Airport_listing()

airport_lister.airport_getter(country_name="Turkey", city_name="Istanbul")

Show_data.data_from_api_getter("airport", True)"""
