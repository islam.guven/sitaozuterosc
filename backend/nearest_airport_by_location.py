import requests
import json

headers = {
    'x-apikey': '3035d833bb6e531654a3cce03e6b1fde',
}

res = requests.get('https://ipinfo.io/')
data = res.json()

city = data['city']

location = data['loc'].split(',')
latitude = location[0]
longitude = location[1]

#print("Latitude : ", latitude)
#print("Longitude : ", longitude)
#print("City : ", city)

url = "https://data-qa.api.aero/data/v3/airports/nearest/" + str(latitude) + "/" + str(longitude) + "?maxAirports=5"

response = requests.get(url, headers=headers)
data = response.text

parsed = str(data)

splitted = parsed.split("{")

airports = []
proper_airport_names = []
proper_airport_codes = []

for airport in splitted:
    if airport != splitted[0] and airport != splitted[1]:
        airports.append(airport)

for proper_airport in airports:
    name = ''
    iata = ''
    x = proper_airport.find("iata")
    if proper_airport[x + 11] != '"':
        y = proper_airport.find("name")
        for index in range(y + 7, len(proper_airport)):
            if proper_airport[index] == '\"':
                break
            else:
                name += proper_airport[index]
        for index2 in range(x + 11 , len(proper_airport)):
            if proper_airport[index2] == '\"':
                break
            else:
                iata += proper_airport[index2]

    if name != '' and iata != '':
        name = name + "(" + iata + ")"
        proper_airport_names.append(name)
        proper_airport_codes.append(iata)

#for propers in proper_airport_names:
 #   print(propers)



