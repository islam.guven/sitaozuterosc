import json
import requests
from backend.airport_listing import Airport_listing
from backend.show_data import  Show_data
from backend import nearest_airport_by_location

import sys


class departing_flights:



    def __init__(self, iata , airport_name):
        self.headers = {
            'x-apikey': '2cfd0827f82ceaccae7882938b4b1627',
        }

        self.airlineCode = []
        self.airline = []
        self.flightNumber = []
        self.status = []
        self.scheduled = []
        self.flight_date = []
        self.flight_time = []
        self.duration = []
        self.airportCode = []
        self.city = []
        self.aircraft = []
        self.terminal = []
        self.gate = []
        self.flight_info = []

        self.arriving_iata = str(self.to_iata_conversion(airport_name))
        self.iata = iata
        self.get_flights(self.iata, self.arriving_iata)



    def url_airports(self, iataCode):
        return 'https://flifo-qa.api.aero/flifo/v3/flights/' + iataCode + '/d?futureWindow=4'

    def get_flights(self, iataCode, arriving_iata):
        response = requests.get(self.url_airports(iataCode), headers=self.headers)
        if response.status_code == 200:
            flights = json.loads(response.content.decode('utf-8'))

            if (flights["success"]):
                for flightInfo in flights["flightRecord"]:
                    if flightInfo["status"] == "SC" and flightInfo["airportCode"] == self.arriving_iata:
                        try:

                            self.airlineCode.append(flightInfo["operatingCarrier"]["airlineCode"])
                        except:
                            self.airlineCode.append("(?)")
                            pass
                        try:
                            self.airline.append(flightInfo["operatingCarrier"]["airline"])
                        except:
                            self.airline.append("(?)")
                            pass
                        try:
                            self.flightNumber.append(flightInfo["operatingCarrier"]["flightNumber"])
                        except:
                            self.flightNumber.append("(?)")
                            pass
                        try:
                            self.status.append(flightInfo["status"])
                        except:
                            self.status.append("(?)")
                            pass
                        try:
                            self.scheduled.append(flightInfo["scheduled"])
                        except:
                            self.scheduled.append("(?)")
                            pass

                        try:
                             t_index = flightInfo["scheduled"].find("T")
                             self.flight_date.append(flightInfo["scheduled"][ : t_index])
                        except:
                            self.flight_date.append("(?)")
                            pass
                        try:
                            self.flight_time.append(flightInfo["scheduled"][t_index + 1 : ])
                        except:
                            self.flight_time.append("(?)")
                            pass
                        try:
                            self.duration.append(flightInfo["duration"])
                        except:
                            self.duration.append("(?)")
                            pass
                        try:
                            self.airportCode.append(flightInfo["airportCode"])
                        except:
                            self.airportCode.append("(?)")
                            pass
                        try:
                            self.city.append(flightInfo["city"])
                        except:
                            self.city.append("(?)")
                            pass
                        try:
                            self.aircraft.append(flightInfo["aircraft"])
                        except:
                            self.aircraft.append("(?)")
                            pass
                        try:
                            self.terminal.append(flightInfo["terminal"])
                        except:
                            self.terminal.append("(?)")
                            pass
                        try:
                            self.gate.append(flightInfo["gate"])
                        except:
                            self.gate.append("(?)")
                            pass

            else:
                self.flight_info.append("No Flight Found!")


            for i in range(0, len(self.flightNumber)):
                self.flightNumber[i] = self.airlineCode[i] + self.flightNumber[i]

                self.flight_info.append(
                    "There is a flight called " + str(self.flightNumber[i]) + " departing from " + str(
                        self.iata) + " (" + str(self.to_name_conversion(self.iata)) + ")"
                    + " and landing to " + self.arriving_iata + " (" + str(
                        self.to_name_conversion(self.arriving_iata)) + ")" + " at " + str(
                        self.flight_time[i]) + " on " + str(self.flight_date[i])
                    + ". Flight will take about " + str(self.duration[i]) + " minutes and the gate is " + str(
                        self.terminal[i]) + str(self.gate[i]) + ".")
                print(self.flight_info[i])






    @staticmethod
    def to_iata_conversion(airport_name):
        with open('backend\\airports_with_cities.json') as json_file:
            airports = json.load(json_file)

        for key in airports.keys():
            for airport in airports[str(key)]:
                if airport_name == airport["name"]:
                    return airport["iataCode"]

    @staticmethod
    def to_name_conversion(airport_name):
        with open('backend\\airports_with_cities.json') as json_file:
            airports = json.load(json_file)

        for key in airports.keys():
            for airport in airports[str(key)]:
                if airport_name == airport["iataCode"]:
                    return airport["name"]
    print("")
if __name__ == '__main__':
    obj = departing_flights(nearest_airport_by_location.proper_airport_codes[0], "Istanbul Sabiha Gökçen International Airport")
