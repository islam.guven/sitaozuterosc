function getAirports(airport){
    $.ajax({
        url: "/get_airports",
        type: "POST",
        dataType: "json",
        contentType: 'application/json;charset=UTF-8',
        data: JSON.stringify(airport, null, '\t'),
        success: function(data){
            $(accordionExample).replaceWith(data)
        }
    });
}

function loadWeatherData(airport){
    var city = $( "#city_div option:selected" ).text();
    var data = {};
    data['city'] = city;
    data['airport'] = airport;
    $.ajax({
        url: "/get_weather",
        type: "POST",
        dataType: "json",
        contentType: 'application/json;charset=UTF-8',
        data: JSON.stringify(data, null, '\t'),
        success: function(data){
            $(weather_data).replaceWith(data)
        }
    });
}

function updateCard(){
    var city = $( "#city_div option:selected" ).text();
    $.ajax({
        url: "/update_cards",
        type: "POST",
        dataType: "json",
        contentType: 'application/json;charset=UTF-8',
        data: JSON.stringify(city, null, '\t'),
        success: function(data){
            $(description_card).replaceWith(data)
        }
    });
}

function getAirport(selectObject) {
    const airport = selectObject.value;
    console.log(airport);
    getAirports(airport);
    loadWeatherData(airport);
    updateCard()
}

