function loadNewCitiesAndAirports(value){
    $.ajax({
        url: "/update_country",
        type: "POST",
        dataType: "json",
        contentType: 'application/json;charset=UTF-8',
        data: JSON.stringify(value, null, '\t'),
        success: function(data){
            $(city_div).replaceWith(data)
        }
    });
    $.ajax({
        url: "/reset_airports",
        type: "POST",
        dataType: "json",
        success: function(data){
            $(airport_div).replaceWith(data)
        }
    });
}

function showCountries(selectObject){
    const value = selectObject.value;
    console.log(value);
    loadNewCitiesAndAirports(value)
}

function loadNewAirports(city){
    var country = $( "#country_div option:selected" ).text();
    var data = {};
    data['city'] = city;
    data['country'] = country
    $.ajax({
        url: "/update_city",
        type: "POST",
        dataType: "json",
        contentType: 'application/json;charset=UTF-8',
        data: JSON.stringify(data, null, '\t'),
        success: function(data){
            $(airport_div).replaceWith(data)
        }
    });
}

function showCities(selectObject){
    const city = selectObject.value;
    console.log(city);
    loadNewAirports(city);
}


