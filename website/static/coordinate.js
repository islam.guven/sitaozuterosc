key = '6d2933d29fcdc5188f5ebc9b61115435';

function getCoordinates() {
    var options = {
        enableHighAccuracy: true,
        timeout: 5000,
        maximumAge: 0
    };

    function success(pos) {
        var crd = pos.coords;
        var lat = crd.latitude.toString();
        var lng = crd.longitude.toString();
        var coordinates = [lat, lng];
        console.log(`Latitude: ${lat}, Longitude: ${lng}`);
        getWeather(coordinates);
        return;
    }

    function error(err) {
        console.warn(`ERROR(${err.code}): ${err.message}`);
    }

    navigator.geolocation.getCurrentPosition(success, error, options);
}

function getWeather(coordinates) {
    var xhr = new XMLHttpRequest();
    var lat = coordinates[0];
    var lng = coordinates[1];

    xhr.open('GET', "https://api.openweathermap.org/data/2.5/weather?lat=" + lat + "&lon=" + lng +
        "&appid=" + key, true);
    xhr.send();
    xhr.onreadystatechange = processRequest;
    xhr.addEventListener("readystatechange", processRequest, false);

    function processRequest(e) {
        if (xhr.readyState == 4 && xhr.status == 200) {
            var response = JSON.parse(xhr.responseText);
            var description = response.weather.main;
            var current_temp = response.main.temp;
            var max_temp = response.main.temp_max;
            var min_temp = response.main.temp_min;
            var country_code = response.sys.country;
            var city = response.name;
            if(city != null && country_code != null)
                document.getElementById("location").innerHTML = city + ', ' + country_code;
            if(current_temp != null)
            {
                current_temp -= 273.15;
                document.getElementById("temperature").innerHTML = parseInt(current_temp) + "&#8451";
            }
            if(description != null)
                document.getElementById("description").innerHTML = description;
            if(max_temp != null && min_temp != null){
                max_temp -= 273.15;
                min_temp -= 273.15;
                document.getElementById("temp_scale").innerHTML = parseInt(min_temp) + '&#8451; / ' + parseInt(max_temp) + '&#8451;';
            }
            getDate()
            console.log('Description: ' + description);
            console.log('Current temp: ' + current_temp);
            console.log('Max temp: ' + max_temp);
            console.log('Min temp: ' + min_temp);
            console.log('Country code: ' + country_code);
            console.log('City: ' + city);
            return;
        }
    }
}

getCoordinates();

function getDate(){
    var today = new Date();
    var dd = String(today.getDate()).padStart(2, '0');
    var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
    var yyyy = today.getFullYear();
    today = mm + '/' + dd + '/' + yyyy;
    document.getElementById("date").innerHTML = today;
}