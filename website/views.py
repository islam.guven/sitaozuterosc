from flask import Blueprint, render_template, jsonify, request, redirect, url_for, session
from flask_login import login_required, current_user
import requests
import json
from backend import nearest_airport_by_location
from backend.flights import departing_flights
from backend.recommendation_weather import recommendation_weather
from backend import Wiki

views = Blueprint('views', __name__)

url = "https://restcountries.eu/rest/v2/"
response = requests.get(url)
data = response.text
parsed = json.loads(data)
all_countries = []
all_codes = []
all_regions = []
codes = []

for country in parsed:
    all_countries.append(country["name"])
    all_codes.append(country["alpha2Code"])

card_infos = [['Paris',
               'Paris (nicknamed the "City of light") is the capital city of France, and the largest city in France. The area is 105 square kilometres (41 square miles), and around 2.15 million people live there. If suburbs are counted, the population of the Paris area rises to 12 million people. The Seine river runs through the oldest part of Paris, and divides it into two parts, known as the Left Bank and the Right Bank. It is surrounded by many forests. Paris is also the center of French economy, politics, traffic and culture. Paris has many art museums and historical buildings. As a traffic center, Paris has a very good underground subway system (called the Metro). It also has two airports. The Metro was built in 1900, and its total length is more than 200 km (120 mi). The city has a multi-cultural style, because 20% of the people there are from outside France. There are many different restaurants with all kinds of food. Paris also has some types of pollution like air pollution and light pollution.',
               '../static/images/paris.jpg'],
              ['Vienna',
               'Vienna is known for its high quality of life. In a 2005 study of 127 world cities, the Economist Intelligence Unit ranked the city first (in a tie with Vancouver and San Francisco) for the world\'s most livable cities. Between 2011 and 2015, Vienna was ranked second, behind Melbourne. In 2018, it replaced Melbourne as the number one spot and continued as the first in 2019. For ten consecutive years (2009–2019), the human-resource-consulting firm Mercer ranked Vienna first in its annual "Quality of Living" survey of hundreds of cities around the world. Monocle\'s 2015 "Quality of Life Survey" ranked Vienna second on a list of the top 25 cities in the world "to make a base within."The UN-Habitat classified Vienna as the most prosperous city in the world in 2012/2013. The city was ranked 1st globally for its culture of innovation in 2007 and 2008, and sixth globally (out of 256 cities) in the 2014 Innovation Cities Index, which analyzed 162 indicators in covering three areas: culture, infrastructure, and markets. Vienna regularly hosts urban planning conferences and is often used as a case study by urban planners. Between 2005 and 2010, Vienna was the world\'s number-one destination for international congresses and conventions. It attracts over 6.8 million tourists a year.',
               '../static/images/vienna.jpg'],
              ['Venice',
               'Venice is a city in Italy. It is the capital of the Veneto region, which is in the north-east of the country. The population of the \'Comune di Venezia\', which is Venice, its lagoon and its mainland is 271,367. Area is 412 km². The population of Venice itself keeps on shrinking at an alarming rate and is now under 55000 locals. Venice is built on 118 small islands that are separated by 150 canals. People cross the canals by many small bridges. They can also navigate across the city on boats, both rowing boats and motor boats. The most famous Venetian type of boat called a gondolas. The buildings in Venice are very old and attractive, and tourists come from all over the world to see them and the canals. This has made Venice one of the most famous cities in the world. The most famous sights are the Rialto Bridge, St Mark\'s Basilica and the Doge\'s Palace. It is important to remember that Venice is much more than few landmarks and that we owe the city a lot: from words to objects or services of our daily life. Its lifestyle and culture are unique in the world and it can get confusing at first.',
               '../static/images/venice.jpg'],
              ['Nevsehir',
               'Nevşehir (from the Persian compound نو شهر Naw-shahr meaning "new city"), formerly Neapolis and Muşkara, is a city and the capital district of Nevşehir Province in the Central Anatolia Region of Turkey. According to the 2010 census, the population of the district is 117,890 of which 85,634 live in the city of Nevşehir. The district covers an area of 535 km2 (207 sq mi), and the town lies at an elevation of 1,224 m (4,016 ft).',
               '../static/images/nevsehir.jpg'],
              ['New York City',
               'New York City (NYC) or simply New York (NY) is the largest city by population in the United States, located in the state of New York. New York’s population is similar to London in the United Kingdom with over 8 million people currently living in it, and over 22 million people live in the bigger New York metropolitan area. It is in the south end of the state of New York, which is in the northeastern United States. It is the financial capital of the US since it is home to the nation\'s stock market, Wall Street, and the One World Trade Center. A leading global city; New York exerts culture, media and capital internationally, as well as attracting great numbers of international travelers. It is also the home of the United Nations Headquarters.',
               '../static/images/newyork.png'],
              ['Agra',
               'Agra is a city on the banks of the Yamuna river in the Agra district of the Indian state of Uttar Pradesh. It is 206 kilometres (128 mi) south of the national capital New Delhi. Agra is the fourth-most populous city in Uttar Pradesh and 24th in India. Sikandar Lodi was the first ruler of the Delhi Sultanate to move his capital from Delhi to Agra in 1504, and so he is regarded as being the founder of Agra. Sikandar Lodi\'s son, Ibrahim Lodi, was defeated at the Battle of Panipat in 1526 by Babur, which marked the beginning of Mughal Empire. In a brief interruption in Mughal rule between 1540 and 1556, Sher Shah Suri, established the short lived Sur Empire. Agra was the capital of the Mughal Empire from 1556 to 1648, under the Mughal Emperors Akbar, Jahangir and Shah Jahan, after which Shah Jahan shifted the capital to Delhi. The Mughal Empire saw the building of many monuments, especially Taj Mahal. The city was later taken by the Jats and then Marathas and later still fell to the British Raj.',
               '../static/images/agra.jpg']]


@views.route('/')
@login_required
def home():
    return render_template("home.html", user=current_user, card_infos=card_infos)


@views.route('/choose_place')
@login_required
def choose_place():
    # closest airport
    headers = {
        'x-apikey': '3035d833bb6e531654a3cce03e6b1fde',
    }

    res = requests.get('https://ipinfo.io/')
    data = res.json()

    city = data['city']

    location = data['loc'].split(',')
    latitude = location[0]
    longitude = location[1]

    print("Latitude : ", latitude)
    print("Longitude : ", longitude)
    print("City : ", city)

    url = "https://data-qa.api.aero/data/v3/airports/nearest/" + str(latitude) + "/" + str(longitude) + "?maxAirports=5"

    response = requests.get(url, headers=headers)
    data = response.text

    parsed = str(data)

    splitted = parsed.split("{")

    airports = []
    proper_airport_names = []
    proper_airport_codes = []

    for airport in splitted:
        if airport != splitted[0] and airport != splitted[1]:
            airports.append(airport)

    for proper_airport in airports:
        name = ''
        iata = ''
        x = proper_airport.find("iata")
        if proper_airport[x + 11] != '"':
            y = proper_airport.find("name")
            for index in range(y + 7, len(proper_airport)):
                if proper_airport[index] == '\"':
                    break
                else:
                    name += proper_airport[index]
            for index2 in range(x + 11, len(proper_airport)):
                if proper_airport[index2] == '\"':
                    break
                else:
                    iata += proper_airport[index2]

        if name != '' and iata != '':
            name = name + " (" + iata + ")"
            proper_airport_names.append(name)
            proper_airport_codes.append(iata)

    # closest airport done
    countries = []
    with open("backend\\airports_with_cities.json", "r") as read_file:
        data = json.load(read_file)
        for country in data.keys():
            countries.append(country)
    weather_data = ['21.03.2021, 12derece', '22.03.2021, 8derece']

    return render_template("choose_place.html", user=current_user, countries=sorted(countries),
                           closest_airports=proper_airport_names, length=len(proper_airport_names),
                           weather_data=weather_data, city_name="", fligth_info=[])


@views.route('/update_city', methods=['POST'])
def updatecity():
    index = 0
    indexes = []
    airports = []
    ct = request.json['city']
    cn = request.json['country']
    with open("backend\\airports_with_cities.json", "r") as read_file:
        a = json.load(read_file)
        for i in a[str(cn)]:
            if i["city"] == ct:
                indexes.append(index)
            index = index + 1
        for i in indexes:
            airports.append(a[str(cn)][i]["name"])
    temp_list = []

    for i in airports:
        if i not in temp_list:
            temp_list.append(i)

    airports = temp_list
    airports.sort()
    return jsonify('', render_template('airport_div_model.html', airports=airports))


@views.route('/update_country', methods=['POST'])
def updatecountry():
    cities = []
    cnt = request.json
    print(cnt)
    with open("backend\\airports_with_cities.json", "r") as read_file:
        a = json.load(read_file)
        for i in a[cnt]:
            cities.append(i["city"])
    temp_list = []

    for i in cities:
        if i not in temp_list:
            temp_list.append(i)
    cities = temp_list
    cities.sort()
    return jsonify('', render_template('city_div_model.html', cities=cities))


@views.route('/reset_airports', methods=["POST"])
def resetairports():
    return jsonify('', render_template('reset_airport_div_model.html'))


@views.route('/get_airports', methods=['POST'])
def getairport():
    dep_fl_info = []
    airport = request.json
    for code in nearest_airport_by_location.proper_airport_codes:
        dep_fl_info.append(departing_flights(code, airport).flight_info)
    return jsonify('', render_template('accordion_model.html', flight_info=dep_fl_info, length=len(dep_fl_info),
                   closest_airports=nearest_airport_by_location.proper_airport_names))


@views.route('/get_weather', methods=['POST'])
def get_weather():
    city_name = request.json['city']
    airport_name = request.json['airport']
    obj = recommendation_weather(airport_name)
    weather_data = obj.weather_forecast
    return jsonify('', render_template('weather_data_model.html', weather_data=weather_data, city_name=city_name))


@views.route('/update_cards', methods=['POST'])
def update_cards():
    city_name = request.json
    description = Wiki.wiki(city_name)
    return jsonify('', render_template('card_model.html', city_name=city_name, description=description))