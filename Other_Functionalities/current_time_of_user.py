from datetime import datetime
import pytz
from timezonefinder import TimezoneFinder
from backend import nearest_airport_by_location


class timeManager:

    def __timezone_finder_wrt_lat_long(self,lat,long):
        tf = TimezoneFinder()
        latitude, longitude = lat,long
        #print(tf.timezone_at(lng=longitude, lat=latitude))
        return tf.timezone_at(lng=longitude, lat=latitude)

    def get_current_time_wrt_timezone(self,lat,long):
        try:
            timezone=self.__timezone_finder_wrt_lat_long(lat,long)
            tz_NY = pytz.timezone(timezone)
            datetime_NY = datetime.now(tz_NY)
            mydate = datetime("2021-03-20T20:55:00+0300")
            value = datetime_NY.strftime("%H:%M:%S")
            print("Current time at "+timezone+" : ", value)
            return value
        except pytz.NonExistentTimeError:
            print("Input timezone is wrong : "+"'"+timezone+"'")
        except pytz.UnknownTimeZoneError:
            print("Input timezone is not found : "+"'"+timezone+"'")

if __name__ =='__main__':
    obj = timeManager()
    print(obj.get_current_time_wrt_timezone(float(nearest_airport_by_location.latitude),float(nearest_airport_by_location.longitude)))



