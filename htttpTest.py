import GoogleMaps from googlemaps as gmaps

def get_coordinates(address):
    city = '<City Name>, <Country>'
    geocode_result = gmaps.geocode(str(address) +' '+ city)
    if len(geocode_result) > 0:
        return list(geocode_result[0]['geometry']['location'].values())
    else:
        return [np.NaN, np.NaN]