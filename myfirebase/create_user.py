from typing import Optional
from firebase_admin import auth
from firebase_admin.auth import UserRecord
from  myfirebase.initialise_firebase_admin import app                       #DO NOT DELETE!

class  manageUsers:

    def __create(self,email: str, user_id: Optional[str]) -> UserRecord:
        return auth.create_user(email=email, uid=user_id) if user_id else auth.create_user(email=email)

    def __set_password(self,user_id: str, password: str) -> UserRecord:
        return auth.update_user(user_id, password=password)

    def update_email(self,user_id: str, email: str) -> UserRecord:
        updated_user = auth.update_user(user_id, email=email)
        print(f"Updated user email to {updated_user.email}")
        return updated_user

    def update_password(self,user_id: str, password: str):
        user = self.__set_password(user_id,password)
        print(f"Firebase has updated the password for user with user id - {user.uid}")

    def create_user(self,email: str,password:str, user_id: Optional[str]):
        self.new_user: UserRecord = self.__create(email, user_id)
        self.update_password(self.new_user.uid,password)
        print(f"Firebase successfully created a new user with email - {self.new_user.email} and user id - {self.new_user.uid}")
        return self.new_user.uid

    def delete_user(self,user_id: str):
        try:
            print(f"Firebase has deleted user with user id - {user_id}")
            return auth.delete_user(user_id)
        except:
            print("\nError ! User couldn't deleted! \n")




if __name__ == "__main__":

    myobject = manageUsers()

    #myobject.create_user("fbcanyagmur@hotmmail.com",user_id=None)     # CREATING A USER !
    #myobject.create_user("fbcanyagmur1@hotmmail.com",user_id="1")     # CREATING A USER WITH SPECIFIED ID!

    #myobject.update_email("1","deneme@hotmail.com")

    myobject.update_password("1","123456")

    #myobject.delete_user("1")