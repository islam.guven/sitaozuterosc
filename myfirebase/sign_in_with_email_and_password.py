import json
import requests
import pprint

from myfirebase.initialise_firebase_admin import app                          #DO NOT DELETE!


FIREBASE_WEB_API_KEY = "AIzaSyACiMgfMod-JUfjKKizFczjVbtrjlTDYXw"
rest_api_url = f"https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword"



def sign_in_with_email_and_password(email: str, password: str, return_secure_token: bool = True):
    payload = json.dumps({
        "email": email,
        "password": password,
        "returnSecureToken": return_secure_token
    })

    r = requests.post(rest_api_url,
                      params={"key": FIREBASE_WEB_API_KEY},
                      data=payload)

    return r.json()


if __name__ == "__main__":
    token = sign_in_with_email_and_password("deneme@hotmail.com", "123456")
    pprint.pprint(token)