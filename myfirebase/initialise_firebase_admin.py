import firebase_admin
from firebase_admin import credentials
from firebase_admin import exceptions
import sys


try:
    cred = credentials.Certificate(r"C:\Users\TULPAR\Desktop\firebase_key\ozuhackathon-firebase.json")
    app = firebase_admin.initialize_app(cred, {
            'databaseURL': "https://ozuhackathon-default-rtdb.firebaseio.com/"
        })


except firebase_admin.exceptions.UnknownError as error:
    print(error)
    print("\nControl your credentials!\n")
    sys.exit(0)
except FileNotFoundError as error:
    print("\n File not found!\n")
    sys.exit(0)

if __name__ == "__main__":
    print(app)