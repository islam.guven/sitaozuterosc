from firebase_admin import db
import json


from myfirebase.initialise_firebase_admin import app                          #DO NOT DELETE!


class myDataBase:

    def __init__(self):
        self.ref = db.reference("/")

    def write_to_database(self, json_file_string,directory):   #you need to pass json file directly and the directory of  your database as arguments.
        try:
            with open(json_file_string, "r") as f:
                file_contents = json.load(f)
                self.ref = db.reference(directory)
                self.ref.set(file_contents)
        except FileNotFoundError as error:
            print(error)

    def jprint(self, obj):
        # create a formatted string of the Python JSON object
        text = json.dumps(obj, sort_keys=True, indent=4)
        print(text)

    def write_to_database_json_string(self, json_string,directory):
        self.ref = db.reference(directory)
        self.ref.set(json_string)
        self.jprint(self.ref.get())

    def read_from_database(self, directory_name):
        self.ref = db.reference(directory_name)
        self.jprint(self.ref.get())



if __name__ == "__main__":
    obj = myDataBase()
    #obj.write_to_database(r"C:\Users\TULPAR\Desktop\sitaozuterosc\myjson.json","/denemee")
    obj.read_from_database("/denemee")

