# I have downloaded flask, flask-login and flask-sqlalchemy (probably you'll need to 'pip install' these packages)
# You need to run main.py and automatically the database.db file will be created
# And after sign-up please dont commit the database.db because it's special for everyone (not a big problem if you push)
# I used sqlalchemy because that's what I see on the internet we can change it if you like it's just for trial
# For now you can sign-up, login and logout

from website import create_app

app = create_app()

if __name__ == '__main__':
    app.run(debug=True)
