import math
from math import radians, cos, sin, asin, sqrt
import json
import requests






class distanceCalculator:


    def __init__(self):
        self.headers = {
            'x-apikey': '3035d833bb6e531654a3cce03e6b1fde',
        }

    def haversine(self,lon1, lat1, lon2, lat2):
        """
        Calculate the great circle distance between two points
        on the earth (specified in decimal degrees)
        """
        # convert decimal degrees to radians
        lon1, lat1, lon2, lat2 = map(radians, [lon1, lat1, lon2, lat2])

        # haversine formula
        dlon = lon2 - lon1
        dlat = lat2 - lat1
        a = sin(dlat / 2) ** 2 + cos(lat1) * cos(lat2) * sin(dlon / 2) ** 2
        c = 2 * asin(sqrt(a))
        r = 6371  # Radius of earth in kilometers. Use 3956 for miles
        return c * r

    def find_distance_between_points(self,p1 :[float], p2 : [float]):
        return math.sqrt((p1[1]-p2[1])**2+(p1[0]-p2[0])**2)

    def __make_url(self,a1:str,a2:str):
        return 'https://data-qa.api.aero/data/v3/airports/distance/'+a1+'/'+a2+'?=&units=km'

    def distance_between_two_airports(self,airport1:str,airport2:str):
        response = requests.get(self.__make_url(airport1,airport2), headers=self.headers)
        if response.status_code == 200:
            airport_json = json.loads(response.content.decode('utf-8'))
        if (airport_json["success"]):
            return airport_json["distance"]






if __name__ == '__main__':

    obj =distanceCalculator()
    #print(obj.find_distance_between_points([0,3],[4,0]))
    #print(obj.haversine(28.992875047515515,41.05429842133585,29.259223240370446,41.029581924068445))
    print(obj.distance_between_two_airports('PAR','TYO'))